<?php

namespace Ademti\Crfw\Campaigns;

use Ademti\Crfw\Settings;
use Ademti\Crfw\Cart;
use Ademti\Crfw\CartEvent;
use Ademti\Crfw\CartTemplate;
use Ademti\Crfw\Campaigns\AbstractCampaign;

class SimpleCampaign extends AbstractCampaign {

	/**
	 * Initialise.
	 *
	 * Set the slug and label.
	 */
	public function init() {
		$this->slug  = 'crfw_simple';
		$this->label = __( 'Simple campaign', 'crfw' );
		// Set unrecovered timeout to 2 days.
		$this->unrecovered_timeout = apply_filters( 'crfw_simple_campaign_unrecovered_timeout', 172800 );
		add_filter( 'crfw_settings_tabs', array( $this, 'settings_tabs' ) );
		add_action( 'crfw_settings_form', array( $this, 'settings_form' ) );
	}

	/**
	 * Register our settings tab.
	 *
	 * @param  array $tabs  Array of tab slugs and labels.
	 *
	 * @return array        Modified array of tab slugs and labels.
	 */
	public function settings_tabs( $tabs ) {
		$tabs['email'] = array(
			'label' => __( 'Simple Campaign', 'crfw' ),
			'callback' => array( $this, 'settings_page' ),
			);
		return $tabs;
	}

	/**
	 * Register our settings.
	 */
	public function settings_form() {
		// Email settings tab.
		register_setting( 'crfw_email_plugin_page', 'crfw_settings_email' );
		add_settings_section(
			'crfw_email_section',
			'',
			null,
			'crfw_email_plugin_page'
			);
		add_settings_field(
			'crfw_email_subject',
			__( 'Email subject line', 'crfw' ),
			array( $this, 'email_subject_render' ),
			'crfw_email_plugin_page',
			'crfw_email_section'
			);
		add_settings_field(
			'crfw_email_content',
			__( 'Email content', 'crfw' ),
			array( $this, 'email_content_render' ),
			'crfw_email_plugin_page',
			'crfw_email_section'
			);

	}

	/**
	 * Render the settings page for the Simple Campaign.
	 */
	public function settings_page() {
		settings_fields( 'crfw_email_plugin_page' );
		do_settings_sections( 'crfw_email_plugin_page' );
		do_action( 'crfw_email_plugin_page' );
		submit_button();
	}

	/**
	 * Render the email subject input box.
	 */
	public function email_subject_render() {
		?>
		<input type='text' name='crfw_settings_email[crfw_email_subject]' size="60" value='<?php esc_attr_e( $this->settings->crfw_email_subject ); ?>'>
		<?php
	}

	/**
	 * Render the textarea editor for the email content input.
	 */
	public function email_content_render() {
		wp_editor(
			$this->settings->crfw_email_content,
			'crfw_settings_email',
			array(
				'textarea_name' => 'crfw_settings_email[crfw_email_content]',
				)
			);
	}

	/**
	 * Run the campaign.
	 */
	public function run_campaign() {
		$cart_ids = $this->get_carts_to_email();
		foreach ( $cart_ids as $cart_id ) {
			$this->send_campaign( $cart_id );
		}
		$this->mark_carts_as_unrecovered();
	}

	/**
	 * Get the carts to email.
	 *
	 * @return array  Array of cart IDs.
	 */
	private function get_carts_to_email() {
		global $wpdb;

		return $wpdb->get_col(
			$wpdb->prepare(
				"SELECT c.id
				FROM {$wpdb->prefix}crfw_cart c
				LEFT JOIN {$wpdb->prefix}crfw_cart_meta m
				ON c.id = m.cart_id
				AND m.name = 'recovery_started'
				LEFT JOIN {$wpdb->prefix}crfw_cart_meta m2
				ON c.id = m2.cart_id
				AND m2.name = 'simple_campaign_sent'
				WHERE c.status = 'recovery'
				AND m2.name IS NULL
				AND m.value < %d
				",
				time() - 1800
				)
			);
	}

	/**
	 * Get carts that have passed the campaign time and unrecoverd threshold.
	 *
	 * @return array  Array of cart IDs.
	 */
	private function get_unrecovered_carts() {
		global $wpdb;

		return $wpdb->get_col(
			$wpdb->prepare(
				"SELECT c.id
				FROM {$wpdb->prefix}crfw_cart c
				LEFT JOIN {$wpdb->prefix}crfw_cart_meta m
				ON c.id = m.cart_id
				AND m.name = 'recovery_started'
				WHERE c.status = 'recovery'
				AND m.value < %d
				",
				time() - 1800 - $this->unrecovered_timeout
				)
			);
	}

	/**
	 * Mark carts as unrecovered.
	 */
	private function mark_carts_as_unrecovered() {
		$cart_ids = $this->get_unrecovered_carts();
		foreach ( $cart_ids as $cart_id ) {
			$cart = new Cart( $cart_id );
			$cart->status = 'unrecovered';
			$cart->save();
		}
	}

	/**
	 * Send the email about a specific cart.
	 *
	 * @param  int $cart_id  The cart ID.
	 */
	private function send_campaign( $cart_id ) {
		// Check if we're enabled. If not - we're done.
		if ( ! $this->settings->crfw_recover_checkout_emails ) {
			return;
		}

		$cart          = new Cart( $cart_id );
		$cart_template = new CartTemplate( $cart, $this->settings, $this->settings->crfw_email_subject );
		$msg           = $this->settings->crfw_email_content;
		$msg           = $cart_template->replace( $msg );
		$headers	   = array(
			'Content-type: text/html',
			'From: ' . $this->settings->crfw_email_from . ' <' . $this->settings->crfw_email_from_address . '>'
			);
		$sent          = $this->settings->engine->mail(
			$cart->email,
			$this->settings->crfw_email_subject,
			$msg,
			$headers
			);
		if ( $sent ) {
			// Update the meta.
			$cart->add_meta( 'simple_campaign_sent', time() );
			$cart->save();
			// Log event.
			$event          = new CartEvent();
			$event->cart_id = $cart_id;
			$event->type    = 'neutral';
			$event->details = __( 'Simple campaign sent.', 'crfw' );
			$event->save();
		}
	}
}
