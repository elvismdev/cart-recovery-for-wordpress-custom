<?php

namespace Ademti\Crfw;

use Ademti\Crfw\Settings;
use Ademti\Crfw\TemplateLoader;

/**
 * Status page class.
 */
class StatusPage {

	private $settings;

	/**
	 * Constructor.
	 *
	 * Enqueue scripts.
	 *
	 * @param Settings $settings Settings object.
	 */
	public function __construct( Settings $settings ) {
		$this->settings = $settings;
		wp_register_script(
			'd3',
			$this->settings->base_url .  '/js/d3.min.js',
			array( 'jquery' ),
			CRFW_VERSION,
			true
		);
		wp_register_script(
			'c3',
			$this->settings->base_url . '/js/c3.min.js',
			array( 'd3' ),
			CRFW_VERSION,
			true
		);
		wp_register_script(
			'crfw-admin',
			$this->settings->base_url . '/js/cart-recovery-for-wordpress-admin.js',
			array( 'c3' ),
			CRFW_VERSION,
			true
		);
		wp_enqueue_script( 'crfw-admin' );
		wp_enqueue_style(
			'c3',
			$this->settings->base_url . '/css/c3.min.css',
			array(),
			CRFW_VERSION
		);
	}

	/**
	 * Render the content for the page.
	 */
	public function render() {
		$variables = $this->get_summary_stats();
		$template_loader = new TemplateLoader();
		$template_loader->output_template_with_variables( 'admin', 'status-page', $variables );
		$this->status_graph();
	}

	/**
	 * Generate summary stats for all open carts in the last 28 days.
	 *
	 * @return Array  Array of counts per status.
	 */
	private function get_summary_stats() {
		global $wpdb;
		$timestamp  = gmmktime( 0, 0, 0 );
		$timestamp  = $timestamp - ( 86400 * 28 );
		$summary = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT status,
						COUNT(id) AS count
				   FROM {$wpdb->prefix}crfw_cart
				  WHERE created > %d
			   GROUP BY status
			    ",
			    $timestamp
			),
			OBJECT_K
		);
		$results = array();
		foreach ( array( 'pending', 'recovery', 'recovered', 'unrecovered' ) as $status) {
			if ( isset( $summary[$status] ) ) {
				$results[ $status . '_cnt' ] = $summary[ $status ]->count;
			} else {
				$results[ $status . '_cnt' ] = 0;
			}
		}
		return $results;
	}

	/**
	 * Status page graph.
	 */
	public function status_graph() {
		global $wpdb, $table_prefix;

		$graph_data                      = new \stdClass();
		$graph_data->data                = new \stdClass();
		$graph_data->data->type          = 'bar';
		$graph_data->data->groups        = array(
			array(
				__( 'Pending', 'crfw' ),
				__( 'Recovery', 'crfw' ),
				__( 'Recovered', 'crfw' ),
				__( 'Unrecovered', 'crfw' ),
			)
		);
		$graph_data->data->columns       = array(
			array( __( 'Pending', 'crfw' ) ),
			array( __( 'Recovery', 'crfw' ) ),
			array( __( 'Recovered', 'crfw' ) ),
			array( __( 'Unrecovered', 'crfw' ) ),
		);
		$graph_data->color               = new \stdClass();
		$graph_data->color->pattern      = array( '#555', '#d46f15', '#191', '#911' );
		$graph_data->legend              = new \StdClass();
		$graph_data->legend->position    = 'right';
		$graph_data->axis                = new \stdClass();
		$graph_data->axis->x             = new \stdClass();
		$graph_data->axis->x->type       = 'category';
		$graph_data->axis->x->categories = array();

		$timestamp  = gmmktime( 0, 0, 0 );
		$timestamp  = $timestamp - ( 86400 * 28 );
		while ( $timestamp < time() ) {
			$stats = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT status,
							COUNT(id) AS count
					   FROM {$table_prefix}crfw_cart
					  WHERE created > %d
					    AND created < %d
				   GROUP BY status
				    ",
				    $timestamp,
				    $timestamp + 86399
				),
				OBJECT_K
			);
			$graph_data->axis->x->categories[] = date( 'd M', $timestamp );
			foreach ( array( 'pending', 'recovery', 'recovered', 'unrecovered' ) as $idx => $status ) {
				if ( isset( $stats[ $status ] ) ) {
					$graph_data->data->columns[ $idx ][] = $stats[ $status ]->count;
				} else {
					$graph_data->data->columns[ $idx ][] = 0;
				}
			}
			$timestamp += 86400;
		}
		wp_localize_script( 'crfw-admin', 'crfw_recovery_graph', $graph_data );
	}
}
