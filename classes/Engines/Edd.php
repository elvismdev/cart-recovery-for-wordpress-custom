<?php

namespace Ademti\Crfw\Engines;

use Ademti\Crfw\Engines\AbstractEngine;
use Ademti\Crfw\Cart;

/**
 * EDD Frontend Engine.
 */
class Edd extends AbstractEngine {

	/**
	 * Store the cart details into on-page JS.
	 */
	public function cart_to_js() {
		$cart = edd_get_cart_contents();
		if ( !$cart ) {
			return;
		}
		$data = array(
			'currency'        => edd_get_currency(),
			'currency_symbol' => edd_currency_symbol(),
			'contents'        => array(),
		);
		foreach ( $cart as $item ) {
			$download  = new \EDD_Download( $item['id'] );
			$image_id  = get_post_thumbnail_id( $item['id'] );
			$image_url = wp_get_attachment_image_src( $image_id, 'thumbnail' );
			$image_url = $image_url[0];
			$js_item = array(
				'name'     => get_the_title( $item['id'] ),
				'price'    => $download->get_price(),
				'image'    => $image_url,
				'quantity' => $item['quantity'],
				'internal' => $item,
			);
			$data['contents'][] = $js_item;
		}
		return $data;
	}

	/**
	 * Repopulate the cart.
	 */
	public function repopulate_cart( Cart $cart ) {

		// Clear any old carts.
		edd_empty_cart();

		$cart_contents = $cart->cart_details['contents'];
		foreach ( $cart_contents as $line_item ) {
			$download_id = $line_item['internal']['id'];
			$option_id   = isset( $line_item['internal']['options']['price_id'] ) ? $line_item['internal']['options']['price_id']  : null;
			$quantity    = $line_item['internal']['quantity'];
			if ( $option_id ) {
				$options = array(
					'price_id' => $option_id,
					'quantity' => $quantity,
				);
			} else {
				$options = array(
					'quantity' => $quantity,
				);
			}
            edd_add_to_cart( $download_id, $options );
	    }
        wp_redirect( $this->get_checkout_url(), '303' );
        exit();
	}

	/**
	 * Get the items in the cart.
	 *
	 * @param  array $cart_details  The cart details stored.
	 * @return array                Array of products and their counts.
	 */
	public function get_cart_items( $cart_details ) {
		if ( empty( $cart_details['contents'] ) ) {
			return array();
		}
		$results = array();
		foreach ( $cart_details['contents'] as $item ) {
			$results[ $item['name'] ] = $item['internal']['quantity'];
		}
		return $results;
	}

	/**
	 * Generate a string describing the value of a cart.
	 */
	public function get_cart_value( $cart_details ) {
		if ( empty( $cart_details['contents'] ) ) {
			return edd_currency_symbol( $cart_details['currency'] ) . '0.00';
		}
		$total = 0;
		foreach ( $cart_details['contents'] as $item ) {
			$total += $item['price'];
		}
		return edd_currency_symbol( $cart_details['currency'] ) . sprintf("%2.2f", $total);
	}

	/**
	 * Get the checkout URL.
	 * @return string  The checkout URL.
	 */
	public function get_checkout_url() {
		return edd_get_checkout_uri();
	}

	/**
	 * Return the suffix for the engine.
	 *
	 * @return string The suffix.
	 */
	protected function get_suffix() {
		return 'edd';
	}

	/**
	 * Determine if the current page is a checkout page.
	 *
	 * @return boolean True if the current page is the checkout page.
	 */
	protected function is_checkout() {
		return edd_is_checkout();
	}

	/**
	 * Register the order completed hook.
	 */
	protected function register_order_completed_action() {
		add_action( 'edd_complete_purchase', array( $this, 'set_cart_completed' ) );
	}

}
