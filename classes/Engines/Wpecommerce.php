<?php

namespace Ademti\Crfw\Engines;

use Ademti\Crfw\Engines\AbstractEngine;
use Ademti\Crfw\Cart;

/**
 * WP e-Commerce Frontend Engine.
 */
class Wpecommerce extends AbstractEngine {

	/**
	 * Store the cart details into on-page JS.
	 */
	public function cart_to_js() {

		global $wpsc_cart;

		if ( ! count( $wpsc_cart->cart_items ) ) {
			return;
		}
		$data = array(
			'currency'        => wpsc_get_currency_code(),
			'currency_symbol' => wpsc_get_currency_symbol(),
			'contents'        => array(),
		);
		foreach ( $wpsc_cart->cart_items as $item ) {
			$image_id  = get_post_thumbnail_id( $item->product_id );
			$image_url = wp_get_attachment_image_src(
				$image_id,
				apply_filters(
					'crfw_image_size',
					'thumbnail'
				)
			);
			$image_url = $image_url[0];
			$js_item = array(
				'price'            => $item->total_price,
				'name'             => $item->product_name,
				'image'            => $image_url,
				'quantity'         => $item->quantity,
				'product_id'       => $item->product_id,
				'variation_data'   => $item->variation_data,
				'variation_values' => $item->variation_values,
			);
			$data['contents'][] = $js_item;
		}
		return $data;
	}

	/**
	 * Repopulate the cart.
	 */
	public function repopulate_cart( Cart $cart ) {
		global $wpsc_cart;

		// Clear any old carts.
		$wpsc_cart->empty_cart();

		// Add the items back into the basket.
		$cart_contents = $cart->cart_details['contents'];
		foreach ( $cart_contents as $line_item ) {
			if (!empty($line_item['variation_values'])) {
				$variation_values = $line_item['variation_values'];
			} else {
				$variation_values = '';
			}
			$parameters = array(
				'variation_values' => $variation_values,
				'quantity'         => $line_item['quantity'],
				'provided_price'   => '',
				'comment'          => '',
				'time_requested'   => '',
				'custom_message'   => '',
				'file_data'        => '',
				'is_customisable'  => '',
				'meta'             => '',
			);
			$wpsc_cart->set_item( $line_item['product_id'], $parameters );
	    }
        wp_redirect( $this->get_checkout_url(), '303' );
        exit();
	}

	/**
	 * Get the items in the cart.
	 *
	 * @param  array $cart_details  The cart details stored.
	 * @return array                Array of products and their counts.
	 */
	public function get_cart_items( $cart_details ) {
		if ( empty( $cart_details['contents'] ) ) {
			return array();
		}
		$results = array();
		foreach ( $cart_details['contents'] as $item ) {
			$results[ $item['name'] ] = $item['quantity'];
		}
		return $results;
	}

	/**
	 * Generate a string describing the value of a cart.
	 */
	public function get_cart_value( $cart_details ) {
		if ( empty( $cart_details['contents'] ) ) {
			return wpsc_get_currency_symbol( $cart_details['currency'] ) . '0.00';
		}
		$total = 0;
		foreach ( $cart_details['contents'] as $item ) {
			$total += $item['price'];
		}
		return wpsc_get_currency_symbol( $cart_details['currency'] ) . sprintf("%2.2f", $total);
	}

	/**
	 * Get the checkout URL.
	 * @return string  The checkout URL.
	 */
	public function get_checkout_url() {
		if ( function_exists( 'get_checkout_url' ) ) {
			return wpsc_get_checkout_url();
		} else {
			return get_option( 'checkout_url' );
		}
	}

	/**
	 * Return the suffix for the engine.
	 *
	 * @return string The suffix.
	 */
	protected function get_suffix() {
		return 'wpecommerce';
	}

	/**
	 * Determine if the current page is a checkout page.
	 *
	 * @return boolean True if the current page is the checkout page.
	 */
	protected function is_checkout() {
		return wpsc_is_checkout();
	}

	/**
	 * Register the order completed hook.
	 */
	protected function register_order_completed_action() {
		add_action( 'wpsc_purchase_log_save', array( $this, 'set_cart_completed' ) );
	}

}
