<?php

namespace Ademti\Crfw\Engines;

use Ademti\Crfw\Engines\AbstractEngine;
use Ademti\Crfw\Cart;

/**
 * WooCommerce Frontend Engine.
 */
class Woocommerce extends AbstractEngine {

	private $cart_to_repopulate = null;

	/**
	 * Store the cart details into on-page JS.
	 */
	public function cart_to_js() {
		global $woocommerce;
		$items = $woocommerce->cart->get_cart();
		if ( ! count( $items ) ) {
			return;
		}
		$data = array(
			'currency'        => get_woocommerce_currency(),
			'currency_symbol' => get_woocommerce_currency_symbol(),
			'contents'        => array(),
		);
		foreach ( $items as $item ) {
			$product   = $item['data'];
			$image_id  = get_post_thumbnail_id( $product->id );
			$image_url = wp_get_attachment_image_src( $image_id, 'thumbnail' );
			$image_url = $image_url[0];
			$js_item   = array(
				'name'           => get_the_title( $product->id ),
				'price'          => round( $item['line_total'], 2),
				'image'          => $image_url,
				'quantity'       => $item['quantity'],
				'product_id'     => $item['product_id'],
				'variation_id'   => $item['variation_id'],
				'variation_data' => $item['variation'],
			);
			$data['contents'][] = $js_item;
		}
		return $data;
	}

	/**
	 * Repopulate the cart.
	 */
	public function repopulate_cart( Cart $cart ) {
		// Store the cart since WooCommerce won't let us do this until wp_loaded.
		$this->cart_to_repopulate = $cart;
		// Hook to say we need to repopulate.
		add_action( 'wp_loaded', array( $this, 'actually_repopulate_cart' ) );
	}

	/**
	 * Actually handle cart re-population.
	 */
	public function actually_repopulate_cart() {

		global $woocommerce;

		// Clear any old carts.
		wc_empty_cart();

		$cart_contents = $this->cart_to_repopulate->cart_details['contents'];
		foreach ( $cart_contents as $line_item ) {
			$product_id     = $line_item['product_id'];
			$variation_id   = $line_item['variation_id'];
			$quantity       = $line_item['quantity'];
			$variation_data = $line_item['variation_data'];
			$woocommerce->cart->add_to_cart(
				$product_id,
				$quantity,
				$variation_id,
				$variation_data
			);
	    }
        wp_redirect( $this->get_checkout_url(), '303' );
        exit();
	}

	/**
	 * Get the items in the cart.
	 *
	 * @param  array $cart_details  The cart details stored.
	 * @return array                Array of products and their counts.
	 */
	public function get_cart_items( $cart_details ) {
		if ( empty( $cart_details['contents'] ) ) {
			return array();
		}
		$results = array();
		foreach ( $cart_details['contents'] as $item ) {
			$results[ $item['name'] ] = $item['quantity'];
			// @TODO Can we add variation info in a generic way?
			// FIXME - needs doing if we're keying by name
		}
		return $results;
	}

	/**
	 * Generate a string describing the value of a cart.
	 */
	public function get_cart_value( $cart_details ) {
		if ( empty( $cart_details['contents'] ) ) {
			return get_woocommerce_currency_symbol( $cart_details['currency'] ) . '0.00';
		}
		$total = 0;
		foreach ( $cart_details['contents'] as $item ) {
			$total += $item['price'];
		}
		return get_woocommerce_currency_symbol( $cart_details['currency'] ) . sprintf("%2.2f", $total);
	}

	/**
	 * Get the checkout URL.
	 * @return string  The checkout URL.
	 */
	public function get_checkout_url() {
		if ( ! function_exists( 'wc_get_checkout_url' ) ) {
			WC()->frontend_includes();
		}
		return wc_get_checkout_url();
	}

	/**
	 * Return the suffix for the engine.
	 *
	 * @return string The suffix.
	 */
	protected function get_suffix() {
		return 'woocommerce';
	}

	/**
	 * Determine if the current page is a checkout page.
	 *
	 * @return boolean True if the current page is the checkout page.
	 */
	protected function is_checkout() {
		return is_checkout();
	}

	/**
	 * Register the order completed hook.
	 */
	protected function register_order_completed_action() {
		// We got an order within WooCommerce
		// This doesn't cover orders that are placed but without payment.
		add_action( 'woocommerce_new_order', array( $this, 'set_cart_completed' ) );
	}

}
