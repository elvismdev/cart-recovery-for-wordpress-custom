<?php

namespace Ademti\Crfw\Engines;

use Ademti\Crfw\Settings;
use Ademti\Crfw\Cart;
use Ademti\Crfw\TemplateLoader;

abstract class AbstractEngine {

	protected $settings;
	protected $suffix;

	/**
	 * Constructor.
	 *
	 * Store the base URL, and add hooks.
	 */
	public function __construct( Settings $settings ) {
		// Store the settings.
		$this->settings = $settings;

		// Enqueue the frontend javascript.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		// Register the AJAX callback handler.
		add_action( 'wp_ajax_nopriv_crfw_record_cart', array( $this, 'record_cart' ) );
		add_action( 'wp_ajax_crfw_record_cart', array( $this, 'record_cart' ) );

		// Register the order completed action.
		$this->register_order_completed_action();

		// Init. Allow cart regeneration.
		add_action( 'init', array( $this, 'init' ) );
	}

	/**
	 * See if we need to re-populate the cart.
	 */
	public function init() {
		// Bail if get arg not present.
		if ( empty( $_GET['crfw_cart_hash'] ) ) {
			return;
		}
		// Validate the hash.
		$cart = new Cart();
		$hash = $cart->load_by_hash( $_GET['crfw_cart_hash'] );

		// If we could not find it - bail.
		if ( $hash != $_GET['crfw_cart_hash'] ) {
			return;
		}
		// Validate that the email matches. Bail if not.
		if ( $cart->email != $_GET['crfw_email'] ) {
			return;
		}

		// Re-populate / freshen the cookie.
		setcookie( 'crfw_cart_hash', $hash, time() + 86400, COOKIEPATH );

		// Do something
		switch ( $_GET['crfw_action'] ) {
			case 'checkout':
				// Repopulate the cart.
				do_action( 'crfw_before_repopulate_cart', $cart );
				$this->repopulate_cart( $cart );
				do_action( 'crfw_after_repopulate_cart', $cart );
				break;
			case 'unsubscribe':
				// Unsubscribe the cart from recovery.
				$cart->unsubscribe();
				$cart->save();
				do_action( 'crfw_after_unsubscribe_cart', $cart );
				add_action( 'wp_footer', array( $this, 'include_unsubscribe_message_template' ) );
				break;
			default;
				// Do nothing.
		}

	}

	/**
	 * Show unsubscribe confirmation.
	 */
	function include_unsubscribe_message_template() {
		wp_enqueue_style(
			'crfw-remodal',
			$this->settings->base_url . "/css/remodal.css",
			array(),
			CRFW_VERSION
		);
		wp_enqueue_style(
			'crfw-remodal-theme',
			$this->settings->base_url . "/css/remodal-default-theme.css",
			array(),
			CRFW_VERSION
		);
		wp_enqueue_script(
			'',
			$this->settings->base_url . "/js/remodal.min.js",
			array('jquery'),
			CRFW_VERSION
		);
		$template_loader = new TemplateLoader();
		$template_loader->output_template_with_variables( 'frontend', 'unsubscribe-message', array() );
	}

	/**
	 * Add the JS to the page if needed.
	 */
	public function enqueue_scripts() {
		if ( ! $this->is_checkout() && empty( $_GET['crfw_action']) ) {
			return;
		}
		$suffix = $this->get_suffix();
		$min    = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		wp_enqueue_script(
			'crfw-' . $suffix,
			$this->settings->base_url . "/js/crfw-{$suffix}{$min}.js",
			array()
		);
		wp_enqueue_script(
			'crfw',
			$this->settings->base_url . "/js/cart-recovery-for-wordpress{$min}.js",
			array()
		);
		$js_info             = $this->cart_to_js();
		$js_info['ajax_url'] = admin_url( 'admin-ajax.php' );
		wp_localize_script( 'crfw', 'crfw_settings', $js_info );
	}

	/**
	 * Update a cart record to indicate it was completed.
	 *
	 * Also clears the cart cookie.
	 */
	public function set_cart_completed() {

		// No cookie. Bail - nothing to do.
		if ( ! isset( $_COOKIE['crfw_cart_hash'] ) ) {
			return;
		} else {
			// Get the cart hash, and clear the cookie.
			$cart_hash = $_COOKIE['crfw_cart_hash'];
			setcookie( 'crfw_cart_hash', null, time() - 10, COOKIEPATH );
		}

		// Update the cart status.
		$cart = new Cart();
		$cart->load_by_hash( $cart_hash );
		$cart->set_completed();
		$cart->save();
		do_action( 'crfw_after_complete_cart', $cart );
	}

	/**
	 * Store the cart in the database.
	 *
	 * AJAX callback from checkout.
	 */
	public function record_cart() {
		// Validate the email address, bail if it doesn't pass.
		if ( ! filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL ) ) {
			return;
		}
		// See if we've already noticed this session.
		$cart_hash = isset( $_COOKIE['crfw_cart_hash'] ) ? $_COOKIE['crfw_cart_hash'] : null;

		// Create a cart object from the hash.
		$cart = new Cart();
		$cart->load_by_hash( $cart_hash );

		// Populate the data.
		$cart->email        = $_POST['email'];
		$cart->first_name   = $_POST['first_name'];
		$cart->surname      = $_POST['surname'];
		$cart->cart_details = $_POST['cart'];
		// We only update the cart update time while it is "pending".
		if ( $cart->status == 'pending' ) {
			$cart->updated      = time();
		}
		$cart_hash          = $cart->save();
		do_action( 'crfw_after_record_cart', $cart );

		// Store the hash back into the cookie / freshen the existing cookie.
		setcookie( 'crfw_cart_hash', $cart_hash, time() + 86400, COOKIEPATH );
	}

	/**
	 * Send an email.
	 */
	public function mail( $to, $subject, $message, $headers ) {
		$template_loader = new TemplateLoader();
		$variables = array(
			'message' => $message
		);
		$message = $template_loader->get_template_with_variables( 'email', 'html-wrapper', $variables );
		return wp_mail( $to, $subject, $message, $headers );
	}

	/**
	 * Store the cart details into on-page JS.
	 */
	abstract public function cart_to_js();

	/**
	 * Get the items in the cart.
	 */
	abstract public function get_cart_items( $cart_details );

	/**
	 * Generate a string describing the value of a cart.
	 */
	abstract public function get_cart_value( $cart_details );

	/**
	 * Repopulate the cart based on a hasehed cart ID in the $_GET vars.
	 */
	abstract public function repopulate_cart( Cart $cart );

	/**
	 * Get the checkout URL.
	 * @return string  The URL.
	 */
	abstract public function get_checkout_url();

	/**
	 * Return the suffix for the engine.
	 *
	 * @return string The suffix.
	 */
	abstract protected function get_suffix();

	/**
	 * Determine if the current page is a checkout page.
	 *
	 * @return boolean True if the current page is the checkout page.
	 */
	abstract protected function is_checkout();

	/**
	 * Add a hook to trigger the order completed action.
	 */
	abstract protected function register_order_completed_action();
}
