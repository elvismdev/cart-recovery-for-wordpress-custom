<?php

namespace Ademti\Crfw;

use \Hashids\Hashids;
use \Ademti\Crfw\CartEvent;

/**
 * Cart class. Represents a cart.
 */
class Cart {

	// The user visible ID.
	private $hashed_id;

	// The actual ID.
	private $cart_id;

	// Whether the record has been modified.
	private $dirty = false;

	private $accessible_props = array(
		'email',
		'status',
		'cart_details',
		'first_name',
		'surname',
		'updated',
		'hashed_id',
	);

	// The main data.
	private $email        = '';
	private $first_name   = '';
	private $surname      = '';
	private $status       = 'pending';
	private $cart_details = array();
	private $created      = 0;
	private $updated      = 0;

	/**
	 * Constructor.
	 */
	public function __construct( $cart_id = null ){
		// This is a new item. Nothing to do here.
		if ( empty( $cart_id ) ) {
			$this->created = time();
			$this->updated = time();
			return;
		}
		// Decode the hash, and load the data.
		$this->load( $cart_id );
	}

	/**
	 * Magic getter.
	 *
	 * Allow access to some properties, which are otherwise set to private so we can
	 * check whether they are dirty.
	 *
	 * @param  string $key  The key to retrieve.
	 *
	 * @return mixed        The value of the key, or NULL;
	 */
	public function __get( $key ) {
		if ( in_array( $key, $this->accessible_props ) ) {
			return $this->$key;
		};
		throw new \Exception('Attempt to access inaccessible property on Cart class.');
	}

	/**
	 * Magic setter.
	 *
	 * Allow access to some properties. Tracks whether items are changed so we know if
	 * we need to save or not.
	 *
	 * @param string  $key   The key to set.
	 *
	 * @param string  $value The value to assign to the property.
	 */
	public function __set( $key, $value ) {
		if ( ! in_array( $key, $this->accessible_props ) ) {
			throw new \Exception('Attempt to set inaccessible property on Cart class.');
		};
		if ( $this->$key !== $value ) {
			$this->$key = $value;
			$this->dirty = true;
		}
	}

	/**
	 * Magic empty() checker.
	 */
	public function __empty( $key ) {
		if ( ! in_array( $key, $this->accessible_props ) ) {
			throw new \Exception('Attempt to set inaccessible property on Cart class.');
		};
		return empty( $this->key );
	}

	/**
	 * Persist the data back to the database.
	 *
	 * @return string  The hashed id.
	 */
	public function save() {
		// Nothing to do unless we're dirty.
		if ( ! $this->dirty ) {
			return $this->hashed_id;
		}
		if ( empty( $this->cart_id ) ) {
			return $this->insert();
		} else {
			return $this->update();
		}
	}

	/**
	 * Update the status to a relevant value when a cart has been completed.
	 *
	 * NOTE: Does not persist changes, the caller is responsible for calling
	 * save() as well to save the status update to the database.
	 */
	public function set_completed() {

		// Note that cart completion tracking is working.
		update_option( 'crfw_cart_completion_working', true );

		// Get the new cart status for the item. Bail if no change.
		$new_status = $this->get_completed_status();
		if ( ! $new_status ) {
			return;
		}
		$this->status = $new_status;
		$this->dirty = true;
	}

	/**
	 * Put cart into recovery process.
	 *
	 * @param  int $pending_duration  The number of seconds the cart was pending for.
	 */
	public function put_in_recovery( $pending_duration ) {
		$this->status = 'recovery';
		$this->dirty  = true;

		// Log event.
		$event          = new CartEvent();
		$event->cart_id = $this->cart_id;
		$event->type    = 'neutral';
		$event->details = __( 'Cart entered recovery process.', 'crfw' );
		$event->save();

		// Store the time that the cart was eligible to go into recovery.
		$this->add_meta( 'recovery_started', $this->updated + $pending_duration );
	}

	/**
	 * Load by the public hashed ID.
	 *
	 * @param  int $hashed_id  The public hashed ID.
	 */
	public function load_by_hash( $hashed_id ) {
		$cart_id = $this->decode( $hashed_id );
		$this->load( $cart_id );
		return $this->hashed_id;
	}

	/**
	 * Add a meta record for this cart.
	 * @param string $name  The meta name.
	 * @param string $value The meta value to store.
	 */
	public function add_meta( $name, $value ) {
		global $wpdb;

		$wpdb->insert(
			$wpdb->prefix . 'crfw_cart_meta',
			array(
				'id' => null,
				'cart_id' => $this->cart_id,
				'name' => $name,
				'value' => $value,
			)
		);
	}

	/**
	 * Unsubscribe a cart from the recovery process.
	 */
	public function unsubscribe() {
		// Only applies if it is in recovery.
		if ( $this->status != 'recovery' ) {
			return;
		}
		// Update the status.
		$this->status = 'unrecovered';
		// Record meta.
		$this->add_meta( 'user_unsubscribe', time() );
		// Log event.
		$event          = new CartEvent();
		$event->cart_id = $this->cart_id;
		$event->type    = 'negative';
		$event->details = __( 'User unsubscribed from emails.', 'crfw' );
		$event->save();
		$this->dirty = true;
	}

	/**
	 * Insert a new object into the database.
	 *
	 * Set the cart_id, hashed_id and return the hashed_id.
	 * @return string  The hashed ID.
	 */
	private function insert() {
		global $wpdb;
		$timestamp = time();
		$res = $wpdb->insert(
			$wpdb->prefix . 'crfw_cart',
			array(
				'id'           => null,
				'email'        => $this->email,
				'first_name'   => $this->first_name,
				'surname'      => $this->surname,
				'status'       => $this->status,
				'cart_details' => serialize( $this->cart_details ),
				'updated'      => $timestamp,
				'created'      => $timestamp,
			)
		);
		$this->updated = $timestamp;
		$this->created = $timestamp;
		if ( ! $res ) {
			throw new \Exception('Could not write cart record.');
		}
		$this->cart_id = $wpdb->insert_id;
		$this->hashed_id = $this->encode( $this->cart_id );

		// Log event.
		$event          = new CartEvent();
		$event->cart_id = $this->cart_id;
		$event->type    = 'positive';
		$event->details = __( 'Cart details captured.', 'crfw' );
		$event->save();

		return $this->hashed_id;
	}

	/**
	 * Update an existing cart entry.
	 *
	 * @return string  The hashed ID.
	 */
	private function update() {
		global $wpdb;
    	$res = $wpdb->update(
			$wpdb->prefix . 'crfw_cart',
			array(
				'email'        => $this->email,
				'first_name'   => $this->first_name,
				'surname'      => $this->surname,
				'status'       => $this->status,
				'updated'      => $this->updated,
				'cart_details' => serialize( $this->cart_details ),
			),
			array(
				'id' => $this->cart_id,
			)
		);
		if ( $res === FALSE ) {
			throw new \Exception( 'Could not update cart record' . print_r($res,1) );
		}
		return $this->hashed_id;
	}

	/**
	 * Work out which status a cart should be updated to when completed.
	 *
	 * @return string         The target status.
	 */
	private function get_completed_status() {
		// If it was completed from pending, then it's a normal purchase. Flag as completed.
		if ( $this->status == 'pending' ) {
			return 'completed';
		// If it was in recovery then we have recovered it!
		} elseif ( $this->status == 'recovery' || $this->status == 'unrecovered' ) {
			return 'recovered';
		// Should not happen, leave status as-is.
		} else {
			return false;
		}
	}

	/**
	 * Load the contents of the cart into the class from the database.
	 *
	 * @param  string $cart_id  The internal ID
	 *
	 * @return bool             True on success. False on failure.
	 */
	private function load( $cart_id ) {

		global $wpdb;

		if ( ! $cart_id ) {
			return FALSE;
		}
		// Try and retrieve the cart from the database.
		$results = $wpdb->get_row(
			$wpdb->prepare(
				"SELECT email,
						first_name,
						surname,
				        status,
				        cart_details,
				        created,
				        updated
				   FROM {$wpdb->prefix}crfw_cart
				  WHERE id = %d",
				$cart_id
			)
		);
		if ( ! $results ) {
			return;
		}
		// If we get here we have cart data. Populate it.
		$this->cart_id      = $cart_id;
		$this->email        = $results->email;
		$this->first_name   = $results->first_name;
		$this->surname      = $results->surname;
		$this->status       = $results->status;
		$this->cart_details = unserialize( $results->cart_details );
		$this->created      = $results->created;
		$this->updated      = $results->updated;
		// Store the IDs since they are valid.
		$this->hashed_id = $this->encode( $cart_id );
	}

	/**
	 * Decode a value
	 *
	 * @param  string      $hashed_id  The hash.
	 *
	 * @return string|bool             The decoded value, or false.
	 */
	private function decode( $hashed_string ) {
		$hashids = new hashids( $this->get_salt() );
		$decoded = $hashids->decode( $hashed_string );
		if ( ! $decoded ) {
			return false;
		} else {
			return $decoded[0];
		}
	}

	/**
	 * Encode a value.
	 *
	 * @param  string $raw_string  The raw string to encode.
	 *
	 * @return string              The encoded string.
	 */
	private function encode( $raw_string ) {
		$hashids = new hashids( $this->get_salt() );
		$encoded = $hashids->encode( $raw_string );
		if ( ! $encoded ) {
			return false;
		} else {
			return $encoded;
		}
	}

	/**
	 * Retrieve, or generate the salt for the hash IDs.
	 *
	 * @return string  The sale to use.
	 */
	private function get_salt() {
		$salt = get_option( 'crfw_cart_salt' );
		if ( ! $salt ) {
			$salt = md5( time() . random_int( 0, 65535 ) );
			add_option( 'crfw_cart_salt', $salt );
		}
		return $salt;
	}
}
