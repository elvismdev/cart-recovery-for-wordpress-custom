<?php

namespace Ademti\Crfw;

use Ademti\Crfw\Settings;
use Ademti\Crfw\Cart;

class CronHandler {

	/**
	 * How long (in seconds) a cart can stay in "pending" before being considered as abandoned.
	 *
	 * Defaults to 1800 seconds. Filterable via crfw_pending_duration.
	 *
	 * @see __construct().
	 *
	 * @var int
	 */
	private $pending_duration;

	/**
	 * How long (in seconds) after the final contact a cart is considered unrecovered.
	 *
	 * Defaults to 172800. Filterable via crfw_unrecovered_duration.
	 *
	 * @var int
	 */
	private $unrecovered_duration;

	/**
	 * Settings instance.
	 *
	 * @var \Ademti\Crfw\\Settings
	 */
	private $settings;

	/**
	 * Constructor.
	 */
	public function __construct( Settings $settings ) {
		$this->settings             = $settings;
		$this->pending_duration     = apply_filters( 'crfw_pending_duration', 1800 );
		$this->unrecovered_duration = apply_filters( 'crfw_unrecovered_duration', 172800 );
	}

	/**
	 * Cron callback.
	 *
	 * Runs all the automation.
	 */
	public function cron() {
		$this->put_pending_carts_into_recovery();
		$this->run_campaigns();
	}

	/**
	 * Update all pending carts older than $pending_duration to be in recovery.
	 */
	private function put_pending_carts_into_recovery() {
		global $wpdb;
		$cart_ids = $wpdb->get_col(
			$wpdb->prepare(
				"SELECT id
		           FROM {$wpdb->prefix}crfw_cart
			      WHERE status = 'pending'
			        AND updated < ( unix_timestamp() - %d )",
			 $this->pending_duration
			)
		);
		foreach ( $cart_ids as $cart_id ) {
			$cart = new Cart( $cart_id );
			$cart->put_in_recovery( $this->pending_duration );
			$cart->save();
		}
	}

	private function run_campaigns() {
		do_action( 'crfw_run_campaigns' );
	}
}
