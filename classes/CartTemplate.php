<?php

namespace Ademti\Crfw;

use \Ademti\Crfw\Cart;
use \Ademti\Crfw\Settings;
use \Ademti\Crfw\TemplateLoader;

/**
 * Cart class. Represents a cart.
 */
class CartTemplate {

	private $cart;
	private $settings;
	private $tags = array();
	private $template_loader;

	/**
	 * Constructor. Store the cart.
	 */
	public function __construct( Cart $cart, Settings $settings, $subject = '' ) {
		$this->cart     = $cart;
		$this->settings = $settings;
		$this->subject  = $subject;
		$this->tags     = array(
			'cart'        => array( $this, 'replace_cart' ),
			'cart_url'    => array( $this, 'replace_cart_url' ),
			'cart_button' => array( $this, 'replace_cart_button' ),
			'last_name'   => array( $this, 'replace_last_name' ),
			'first_name'  => array( $this, 'replace_first_name' ),
			'store_name'  => array( $this, 'replace_store_name' ),
			'store_email' => array( $this, 'replace_store_email' ),
			'unsub_link'  => array( $this, 'replace_unsub_link' ),
			'subject'     => array( $this, 'replace_subject' ),
		);
		$this->template_loader = new TemplateLoader();
	}

	/**
	 * Replace template tags in the input string based on the cart contents.
	 *
	 * @param  string $string  The input string.
	 *
	 * @return string          The string with tags replaced.
	 */
	public function replace( $string ) {
		foreach ( $this->tags as $tag => $callback ) {
			if ( stripos( $string, '{' . $tag . '}' ) !== FALSE ) {
				$string = str_replace( '{' . $tag . '}', $callback(), $string );
			}
		}
		return $string;
	}

	/**
	 * Deal with the {first_name} tag.
	 *
	 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
	 */
	private function replace_first_name() {
		$name = $this->cart->first_name;
		if ( !empty( $name ) ) {
			return $name;
		} else {
			return _x( 'friend', 'crfw', 'Used as a first name if none is available.' );
		}
	}

	/**
	 * Deal with the {surname} tag.
	 *
	 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
	 */
	private function replace_surname() {
		$name = $this->cart->surname;
		if ( !empty( $name ) ) {
			return $name;
		} else {
			return '';
		}
	}

	/**
	 * Deal with the {store_name} tag.
	 *
	 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
	 */
	private function replace_store_name() {
		$name = get_bloginfo( 'name' );
		if ( !empty( $name ) ) {
			return $name;
		} else {
			return '';
		}
	}

	/**
	 * Deal with the {cart} tag.
	 *
	 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
	 */
	private function replace_cart() {
		$output = '';
		$output .= $this->template_loader->get_template_with_variables( 'email', 'cart-header', array() );
		foreach ( $this->cart->cart_details['contents'] as $item ) {
			$variables = array();
			$variables['name']   = $item['name'];
			$variables['price']  = $this->cart->cart_details['currency_symbol'] . ' ';
			$variables['price'] .= sprintf('%.2f', $item['price'] );
			$variables['image']  = $item['image'];
			$variables['width']  = apply_filters( 'crfw_email_image_width', 64 );
			$variables['height'] = apply_filters( 'crfw_email_image_height', 64 );
			$output .= $this->template_loader->get_template_with_variables( 'email', 'cart-row', $variables );
		}
		$output .= $this->template_loader->get_template_with_variables( 'email', 'cart-footer', array() );
		return $output;
	}

	/**
	 * Deal with the {cart_url} tag.
	 *
	 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
	 */
	private function replace_cart_url() {
		// Get the checkout URL from the engine.
		$url = $this->settings->engine->get_checkout_url();
		return add_query_arg(
			array(
				'crfw_cart_hash' => $this->cart->hashed_id,
				'crfw_email'     => $this->cart->email,
				'crfw_action'    => 'checkout',
			),
			$url
		);
	}

	/**
	 * Deal with the {cart_button} tag.
	 *
	 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
	 */
	private function replace_cart_button() {
		// Get the checkout URL from the engine.
		$url = $this->settings->engine->get_checkout_url();
		$variables = array(
			'cart_url' => add_query_arg(
				array(
					'crfw_cart_hash' => $this->cart->hashed_id,
					'crfw_email'     => $this->cart->email,
					'crfw_action'    => 'checkout',
				),
				$url
			)
		);
		return $this->template_loader->get_template_with_variables( 'email', 'cart-button', $variables );
	}

	/**
	 * Deal with the {unsub_link} tag.
	 *
	 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
	 */
	private function replace_unsub_link() {
		$url = home_url();
		return add_query_arg(
			array(
				'crfw_cart_hash' => $this->cart->hashed_id,
				'crfw_email'     => $this->cart->email,
				'crfw_action'    => 'unsubscribe',
			),
			$url
		);
	}

	/**
	 * Replace the subject tag.
	 *
	 * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
	 */
	private function replace_subject() {
		return $this->subject;
	}
}