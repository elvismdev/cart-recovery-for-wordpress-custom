<?php

namespace Ademti\Crfw;

use Ademti\Crfw\Settings;
use Ademti\Crfw\CronHandler;

/**
 * Main plugin class, responsible for triggering everything.
 */
class Main {

	private $base_url;
	private $settings_instance;
	private $admin_instance;
	private $cron_instance;

	private $db_version;

	/**
	 * Constructor
	 */
	public function __construct( $base_url ){
		$this->db_version = CRFW_DB_VERSION;
		$this->base_url   = $base_url;
		add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
		// Has to run after 10 so that settings have been loaded. See Settings::init().
		// Eurgh.
		add_action( 'init', array( $this, 'init' ), 15 );
		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_filter( 'cron_schedules', array( $this, 'cron_schedules' ) );
		add_filter( 'crfw_campaign_classes', array( $this, 'campaign_classes' ) );
	}

	/**
	 * Register a fifteen minute cron interval.
	 *
	 * @param  array $schedules  Array of current schedules.
	 *
	 * @return array             Modified array of schedules.
	 */
	public function cron_schedules( $schedules ) {
		$schedules['crfw5m'] = array(
			'interval' => '300',
			'display'  => __('Every 5 minutes'),
		);
		return $schedules;
	}

	// Allow third parties to get our settings instance.
	public function get_settings() {
		return $this->settings_instance;
	}

	/**
	 * Implements plugins_loaded().
	 */
	public function plugins_loaded() {
		$this->settings_instance = new Settings( $this, $this->base_url );
		if ( empty( $this->settings_instance->engine ) ) {
			return;
		}
		$this->cron_instance     = new CronHandler( $this->settings_instance );
		// Register any campaign classes.
		$campaign_classes = apply_filters( 'crfw_campaign_classes', array() );
		foreach ( $campaign_classes as $class_info ) {
			$class_name = $class_info['class'];
			$this->settings_instance->register_campaign(
				new $class_name( $this->settings_instance, $class_info['data'] )
			);
		}
	}

	/**
	 * Register the built-in campaign.
	 *
	 * @param  array $campaigns  Array of existing campaigns.
	 *
	 * @return array             Modified array of campaigns.
	 */
	public function campaign_classes( $campaigns ) {
		$campaigns[] = array(
			'class' => '\Ademti\Crfw\Campaigns\SimpleCampaign',
			'data'  => array(),
		);
		return $campaigns;
	}

	/**
	 * Ensure that a cron event if scheduled if it needs to be. Schedule one if not.
	 */
	private function ensure_cron_hooked() {
		// Hook the cron callback.
		add_action( 'crfw_cron', array( $this->cron_instance, 'cron' ) );

		// Check for an existing scheduled event. If we have one nothing else to do.
		if ( wp_next_scheduled( 'crfw_cron' ) ) {
			return;
		}

		// No scheduled event found. Add one for ten minutes in the future.
		wp_schedule_event( time() + 6, 'crfw5m', 'crfw_cron' );
	}

	/**
	 * Implements init().
	 *
	 * Set up translation for the plugin.
	 */
	public function init() {
		$locale = apply_filters( 'plugin_locale', get_locale(), 'crfw' );
		load_textdomain( 'crfw', WP_LANG_DIR.'/cart-recovery-for-wordpress/crfw-' . $locale . '.mo' );
		load_plugin_textdomain( 'crfw', false, basename( dirname( __FILE__ ) ) . '/languages/' );

		// Ensure the cron job is hooked.
		$this->ensure_cron_hooked();
	}

	/**
	 * Fires on admin_init().
	 *
	 * Check for any required database schema updates.
	 */
	public function admin_init() {
		$this->check_db_version();
	}

	/**
	 * Check for pending upgrades, and run them if required.
	 */
	public function check_db_version() {
		$current_db_version = (int) get_option( 'crfw_db_version', 1 );
		// Bail if we're already up to date.
		if ( $current_db_version >= $this->db_version ) {
			return;
		}
		// Otherwise, check for, and run updates.
		foreach ( range( $current_db_version + 1, $this->db_version ) as $version ) {
			if ( is_callable( array( $this, 'upgrade_db_to_' . $version ) ) ) {
				$this->{'upgrade_db_to_' . $version}();
				update_option( 'crfw_db_version', $version );
			} else {
				update_option( 'crfw_db_version', $version );
			}
		}
	}

	/**
	 * Output the admin header wrappers.
	 */
	public function admin_header( $active_tab = null ) {
		$this->settings_instance->admin_header( $active_tab );
	}

	/**
	 * Output the admin footer wrappers.
	 */
	public function admin_footer( $active_tab = null ) {
		$this->settings_instance->admin_footer( $active_tab );
	}
}
