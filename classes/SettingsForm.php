<?php

namespace Ademti\Crfw;

use Ademti\Crfw\TemplateLoader;

class SettingsForm {

	/**
	 * Constructor - store the settings class.
	 */
	public function __construct( $settings ) {
		$this->settings        = $settings;
		$this->template_loader = new TemplateLoader();
	}

	/**
	 * Settings callback.
	 *
	 * Hooked onto admin_init by Settings::__construct.
	 *
	 * @see  Settings::__construct
	 *
	 * @return void
	 */
	public function settings_init() {

		// Main settings tab.
		register_setting( 'crfw_main_plugin_page', 'crfw_settings_main' );

		add_settings_section(
			'crfw_main_section',
			'',
			null,
			'crfw_main_plugin_page'
		);

		add_settings_field(
			'crfw_recover_checkout_emails',
			__( 'Send campaigns', 'crfw' ),
			array( $this, 'recover_checkout_emails_render' ),
			'crfw_main_plugin_page',
			'crfw_main_section'
		);
		add_settings_field(
			'crfw_email_from',
			__( 'Email "From" name', 'crfw' ),
			array( $this, 'email_from_render' ),
			'crfw_main_plugin_page',
			'crfw_main_section'
		);
		add_settings_field(
			'crfw_email_from_address',
			__( 'Email "From" address', 'crfw' ),
			array( $this, 'email_from_address_render' ),
			'crfw_main_plugin_page',
			'crfw_main_section'
		);
		do_action( 'crfw_settings_form' );
	}

	public function recover_checkout_emails_render() {
		if ( ! get_option( 'crfw_cart_completion_working' ) ) {
			$disabled = 'disabled="disabled"';
		} else {
			$disabled = '';
		}
		?>
		<input type='checkbox' <?php echo $disabled; ?> name='crfw_settings_main[crfw_recover_checkout_emails]' <?php checked( $this->settings->crfw_recover_checkout_emails, 1 ); ?> value='1'>
		<?php
		if ( ! get_option( 'crfw_cart_completion_working' ) ) {
			$this->template_loader->output_template_with_variables( 'admin', 'enablement-notice', array() );
		}
	}

	/**
	 * Render the email from input box.
	 */
	public function email_from_render() {
		?>
		<input type='text' name='crfw_settings_main[crfw_email_from]' size="40" value='<?php esc_attr_e( $this->settings->crfw_email_from ); ?>'>
		<?php
	}

	/**
	 * Render the email from input box.
	 */
	public function email_from_address_render() {
		?>
		<input type='text' name='crfw_settings_main[crfw_email_from_address]' size="40" value='<?php esc_attr_e( $this->settings->crfw_email_from_address ); ?>'>
		<?php
	}

	/**
	 * Help text for the main settings page.
	 */
	public function help_main() {
		echo __( "<p>On this page you can choose to enable abandoned cart tracking, or not.</p><p><strong>Note:</strong> - if you disable tracking, then no further emails will be sent - even for carts already in the recovery process.</p><p>The available tracking is based on email entry during checkout. When a customer enters their email during checkout then it is stored, together with details of the customer's baseket contents. If the customer doesn't successfully complete an order then they will enter the recovery process.</p>", 'crfw' );
	}

	/**
	 * Help text for the email settings page.
	 */
	public function help_email() {
		echo __( "<p>On this page you can enter the email details that you want to send to customers who don't complete their order. You can specify the From name, and email address, the subject line, and the email content.", 'crfw' );
	}

}