<?php

/*
Plugin Name: Cart recovery for WordPress
Plugin URI: https://wp-cart-recovery.com
Description: An easy-to-use plugin that allows you to capture abandoned, and failed orders, and follow up.
Author: Lee Willis
Version: 1.6.1
Author URI: https://wp-cart-recovery.com/
*/

/**
 * Copyright (c) 2016 Ademti Software Ltd. // www.ademti-software.co.uk
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * **********************************************************************
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

define( 'CRFW_VERSION', '1.6.1' );
define( 'CRFW_DB_VERSION', 2 );

if ( version_compare( phpversion(), '5.5', '<' ) ) {

	add_action( 'admin_init', 'crfw_plugin_deactivate' );
	add_action( 'admin_notices', 'crfw_plugin_admin_notice' );

	function crfw_plugin_deactivate() {
		if ( ! current_user_can( 'activate_plugins' ) ) {
			return;
		}
		deactivate_plugins( plugin_basename( __FILE__ ) );
	}

	function crfw_plugin_admin_notice() {
		if ( ! current_user_can( 'activate_plugins' ) ) {
			return;
		}
		echo '<div class="error"><p><strong>Cart Recovery for WordPress</strong> requires PHP version 5.5 or above.</p></div>';
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}

} else {

	// Add autoloader.
	require_once( 'autoload.php' );

	/**
	 * Install function. Create the table to store the replacements
	 */
	function crfw_install() {
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		// Create the tables we need.
		require_once ABSPATH . 'wp-admin/includes/upgrade.php';

		$table_name = $wpdb->prefix . 'crfw_cart';
		$sql = "CREATE TABLE $table_name (
		            id INT(11) NOT NULL AUTO_INCREMENT,
		            email VARCHAR(2014) NOT NULL,
		            first_name VARCHAR(1024),
		            surname VARCHAR(1024),
		            status VARCHAR(16) NOT NULL DEFAULT 'pending',
		            cart_details TEXT,
		            created INT(11) NOT NULL,
		            updated INT(11) NOT NULL,
		            PRIMARY KEY  (id)
		        ) $charset_collate";
		dbDelta( $sql );

		$table_name = $wpdb->prefix . 'crfw_cart_event';
		$sql = "CREATE TABLE $table_name (
		            id INT(11) NOT NULL AUTO_INCREMENT,
		            cart_id INT(11) NOT NULL,
		            type VARCHAR(255) NOT NULL DEFAULT 'note',
		            details TEXT,
		            created INT(11) NOT NULL,
		            PRIMARY KEY  (id)
		        ) $charset_collate";
		dbDelta( $sql );

		$table_name = $wpdb->prefix . 'crfw_cart_meta';
		$sql = "CREATE TABLE $table_name (
		            id INT(11) NOT NULL AUTO_INCREMENT,
		            cart_id INT(11) NOT NULL,
		            name VARCHAR(128) NOT NULL,
		            value TEXT,
		            PRIMARY KEY  (id),
		            KEY cart_name_idx (cart_id,name)
		        ) $charset_collate";
		dbDelta( $sql );

		// Store options to indicate we're installed and are ready to go.
		update_option( 'crfw_db_version', CRFW_DB_VERSION );
	}
	register_activation_hook( __FILE__, 'crfw_install' );


	function crfw_deactivation() {
		wp_clear_scheduled_hook('crfw_cron');
	}
	register_deactivation_hook(__FILE__, 'crfw_deactivation');

	$GLOBALS['crfw'] = new \Ademti\Crfw\Main( plugins_url( '', __FILE__ ) );
}
