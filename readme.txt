=== Cart recovery for WordPress ===
Contributors: leewillis77
Donate link: https://wp-cart-recovery.com
Tags: e-commerce, marketing, ecommerce, email, recovery, woocommerce, easy digital downloads, abandoned, cart
Requires at least: 4.4
Tested up to: 4.5
License: GPL v2
Stable tag: 1.6.1

Cart recovery for WordPress brings abandoned cart recovery and tracking to your WordPress store.

== Description ==

Cart recovery for WordPress brings abandoned cart recovery and tracking to your WordPress store. Here’s what you can expect from Cart recovery for WordPress:

* Tracks customer names and emails as soon as they’re entered at checkout
* Automated cart recovery emails & cart re-population
* Includes easy to use stats inside WordPress
* WooCommerce, WP e-Commerce and Easy Digital Downloads compatibility out-of-the-box
* The basic plugin lets you track abandoned carts, view stats in your WordPress dashboard, and configure your recovery email.

== Installation ==

* Install it as you would any other plugin
* Activate it
* Head over to Cart Recovery and set up your recovery email

== Screenshots ==

1. Detailed stats available in your WordPress admin area.
2. Configurable HTML email templates
3. Built-in HTML email templates, with tags for personalising your emails

== Changelog ==

= 1.6.1 =
* Now handles updates via WordPress.org

= 1.5.1 =
* Templating fix.

= 1.5 =
* PHP 5.5 compatibility fix. Props @elvismdev

= 1.4 =
* Record events when users unsubscribe.

= 1.3 =
* Integrate with licensing and auto upgrades in the absence of a WordPress.org repo.

= 1.2 =
* Set email subject in HTML email title tag.

= 1.1 =
* Minor fixes to HTML email styling

= 1.0 =
* Initial release