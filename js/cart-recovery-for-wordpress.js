function crfw_record_checkout( email_address, first_name, surname ) {
	data = {
		email:      email_address,
		first_name: first_name,
		surname:    surname,
		cart:       crfw_settings,
		action:     'crfw_record_cart'
	}
	jQuery.post(
		crfw_settings.ajax_url,
		data
	);
}

jQuery(document).ready(function() {
	if ( jQuery('[data-remodal-id=crfw-unsubscribe]').length > 0 ) {
		var crfw_modal = jQuery('[data-remodal-id=crfw-unsubscribe]').remodal();
		crfw_modal.open();
	}
});