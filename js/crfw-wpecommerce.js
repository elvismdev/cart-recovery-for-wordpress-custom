jQuery(document).ready(function() {
	jQuery(document).on(
		'blur',
		'[data-wpsc-meta-key="billingfirstname"],
		 [data-wpsc-meta-key="billinglastname"],
		 [data-wpsc-meta-key="billingemail"]',
		function() {
			crfw_record_checkout(
				jQuery('[data-wpsc-meta-key="billingemail"]').val(),
				jQuery('[data-wpsc-meta-key="billingfirstname"]').val(),
				jQuery('[data-wpsc-meta-key="billinglastname"]').val()
			);
		}
	);
});

