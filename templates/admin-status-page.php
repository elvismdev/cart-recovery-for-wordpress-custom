<div id="dashboard-widgets-wrap">
	<div id="dashboard-widgets" class="metabox-holder">
		<div id="recovery-graph" class="postbox">
			<div class="inside">
				<div class="main">
					<div id="crfw-recovery-graph"></div>
					<div class="crfw-summary-stat first crfw-summary-stat-pending"><p><strong>{pending_cnt}</strong><?php _e( 'Pending' ); ?></p></div>
					<div class="crfw-summary-stat second crfw-summary-stat-recovery"><p><strong>{recovery_cnt}</strong><?php _e( 'In recovery' ); ?></p></div>
					<div class="crfw-summary-stat third crfw-summary-stat-unrecovered"><p><strong>{unrecovered_cnt}</strong><?php _e( 'Abandoned' ); ?></p></div>
					<div class="crfw-summary-stat last crfw-summary-stat-recovered"><p><strong>{recovered_cnt}</strong><?php _e( 'Recovered' ); ?></p></div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
</div>
