<form action="options.php" method='post'>
	<p><?php _e( 'Use this to enable, or disable abandonment tracking, sending of recovery emails, and set your email sender details.', 'crfw' ) ?></p>