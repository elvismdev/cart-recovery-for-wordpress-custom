<div data-remodal-id="crfw-unsubscribe"
  data-remodal-options="hashTracking: false, closeOnOutsideClick: true">
  <h2><?php _e( 'You have been unsubscribed.', 'crfw' ); ?></h2>
  <br>
  <button data-remodal-action="confirm" class="remodal-confirm"><?php _e( 'OK', 'crfw' ); ?></button>
</div>