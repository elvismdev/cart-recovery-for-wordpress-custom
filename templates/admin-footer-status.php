<div class="crfw-pro-banner">
	<h2><a href="https://wp-cart-recovery.com/downloads/cart-recovery-wordpress-pro/?utm_source=wporg&amp;utm_medium=plugin&amp;utm_campaign=crfwproupgrade">Go Pro today and turbo charge your sales recovery</a></h2>
	<p><strong>Don't miss out - grab a Pro add-on today, and <em>save 15%</em> off your first Pro order with the code <code>CARTRECOVERY</code> at the checkout.</strong></p>
	<p><a href="https://wp-cart-recovery.com/downloads/cart-recovery-wordpress-pro/?utm_source=wporg&amp;utm_medium=plugin&amp;utm_campaign=crfwproupgrade" class="button button-primary">Go Pro today &raquo;</a></p>
</div>