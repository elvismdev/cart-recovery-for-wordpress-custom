<div class="crfw-pro-banner">
	<h2>Bag even more sales&hellip;</h2>
	<p>Go Pro today - <a href="https://wp-cart-recovery.com/downloads/cart-recovery-wordpress-pro/?utm_source=wporg&amp;utm_medium=plugin&amp;utm_campaign=crfwproupgrade">Cart Recovery For WordPress Pro</a> is a suite of fully supported add-ons to really turbo charge your sales recovery:
	<ul class="ul-disc">
		<li>Create multi-email campaigns.</li>
		<li>Generate and include dynamic per-customer discount codes.</li>
		<li>View details of carts in realtime in your WordPress admin area.</li>
	</ul>
	<p><strong>Don't miss out - grab a Pro add-on today, and <em>save 15%</em> off your first Pro order with the code <code>CARTRECOVERY</code> at the checkout.</strong></p>
	<p><a href="https://wp-cart-recovery.com/downloads/cart-recovery-wordpress-pro/?utm_source=wporg&amp;utm_medium=plugin&amp;utm_campaign=crfwproupgrade" class="button button-primary">Go Pro today &raquo;</a></p>
</div>