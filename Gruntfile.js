module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		less: {
			development: {
				options: {
					paths: ["css"],
					compress: true,
				},
				files: {
					"css/cart-recovery-for-wordpress-admin.css": "less/cart-recovery-for-wordpress-admin.less"
				}
			}
		},
		uglify: {
			crfwjs: {
				files: {
			        'js/crfw-edd.min.js': ['js/crfw-edd.js'],
			        'js/crfw-woocommerce.min.js': ['js/crfw-woocommerce.js'],
			        'js/crfw-wpecommerce.min.js': ['js/crfw-wpecommerce.js'],
			        'js/cart-recovery-for-wordpress.min.js': ['js/cart-recovery-for-wordpress.js']
			    }
			}
		},
		watch: {
			css: {
				files: [
					'less/cart-recovery-for-wordpress-admin.less'
				],
				tasks: [ 'less' ],
			},
			js: {
				files: [
					'js/cart-recovery-for-wordpress.js',
					'js/crfw-edd.js',
					'js/crfw-woocommerce.js',
					'js/crfw-wpecommerce.js'
				],
				tasks: [ 'uglify' ]
			},
		}
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['less', 'uglify']);

};